#include "sche.h"

void *proceso_general(void *params);
void *programa_ejecutable(void *params);
void *descargar_archivo(void *params);

int main(int argc, char const *argv[]){
	task *t1 = create_tarea(1, UNO, "TAREA_1", "Calculadora", EN_PROCESO, proceso_general);
	task *t2 = create_tarea(1, DOS, "TAREA_2", "Abrir Facebook", DETENIDO, proceso_general);
	task *t3 = create_tarea(3, TRES, "TAREA_3", "Abrir Word", EN_PROCESO, programa_ejecutable);
	task *t4 = create_tarea(4, TRES, "TAREA_4", "Guardar Archivo", EN_PROCESO, proceso_general);
	task *t5 = create_tarea(3, TRES, "TAREA_5", "Descargar Imagen", EN_PROCESO, descargar_archivo);
	task *t6 = create_tarea(4, TRES, "TAREA_6", "Archivo Descargado", DETENIDO, proceso_general);
	task *t7 = create_tarea(0, TRES, "TAREA_7", "Actualizando Datos...", EN_PROCESO, proceso_general);
	task *t8 = create_tarea(2, CUATRO, "TAREA_8", "Abrir VLC", EN_PROCESO, programa_ejecutable);
	task *t9 = create_tarea(5, CUATRO, "TAREA_9", "Abrir Camtacia Studio", DETENIDO, programa_ejecutable);
	task *t10 = create_tarea(1, CUATRO, "TAREA_10", "Subiendo Archivo", EN_PROCESO, proceso_general);

	array_task *array = create_ArrayTasks();

	add_tarea(array, *t1);
	add_tarea(array, *t2);
	add_tarea(array, *t3);
	add_tarea(array, *t4);
	add_tarea(array, *t5);
	add_tarea(array, *t6);
	add_tarea(array, *t7);
	add_tarea(array, *t8);
	add_tarea(array, *t9);
	add_tarea(array, *t10);

	run_tarea(array);

	free(array);
	free(t1);
	free(t2);
	free(t3);
	free(t4);
	free(t5);
	free(t6);
	free(t7);
	free(t8);
	free(t9);
	free(t10);

	return 0;
}

void *programa_ejecutable(void *params){
	char *proceso;
	proceso = (char *)params;
	printf("\t ------> Ejecutando Proceso: [%s]\n", proceso);
	pthread_exit(NULL);
}

void *descargar_archivo(void *params){
	char *proceso;
	proceso = (char *)params;
	printf("\t ------> Ejecutando Proceso: [%s]\n", proceso);
	pthread_exit(NULL);
}

void *proceso_general(void *params){
	char *proceso;
	proceso = (char *)params;
	printf("\t ------> Ejecutando Proceso: [%s]\n", proceso);
	pthread_exit(NULL);
}