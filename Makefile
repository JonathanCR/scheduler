all:
	gcc -g -c -Wall main.c -o main.o
	gcc -g -c -Wall sche.c -o sche.o
	gcc -g -Wall main.o sche.o -o Scheduler -lpthread

clean:
	rm *.o
	rm Scheduler
