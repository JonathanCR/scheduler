#include "sche.h"

static int buscar(array_task *_array);
static void initingArray(array_task *_array);
static bool in_ID_tarea(array_task *_array, task _t);

task *create_tarea(int _id, int _retraso, char *_nomTarea, char *_proceso, int _estado, TAREA_ACCION _accion){
	task *_t = (task *)malloc(sizeof(task));
	_t->id = _id;
	_t->retraso = _retraso;
	strcpy(_t->nomTarea, _nomTarea);
	strcpy(_t->proceso, _proceso);
	_t->estado = _estado;
	_t->accion = _accion;
	return _t;
}

void add_tarea(array_task *_array, task _t){
	int _index = buscar(_array);
	bool id_ok = in_ID_tarea(_array, _t);
	if(_index != -1){
		if(id_ok != false){
			_array[_index].tarea = _t;
			_array[_index].empty = false;
			printf("Se agredo [_array] la tarea [%s] con id [%d] <--------OK\n", _array[_index].tarea.nomTarea, _array[_index].tarea.id);
			contar_tareas++;
		}
		else{
			printf("El ID [%d] ya se encunetra asociado a una Tarea....\n",_t.id);
		}
	}
	else{
		printf("No hay index disponible dentro del proceso...\n");
	}
}

void run_tarea(array_task *_array){
	printf("---->Tarea = [%d]\n", contar_tareas);
	pthread_t tareas[contar_tareas];
	for(int i = 0; i < contar_tareas; i++){
		if (_array[i].tarea.estado == EN_PROCESO){
			printf("------------------------------------------------------------->\n");
			printf("\t La tarea de nombre [%s], con ID [%d], a realizar el proceso [%s] con un retraso de [%d] segundos\n", _array[i].tarea.nomTarea, _array[i].tarea.id, _array[i].tarea.proceso, _array[i].tarea.retraso);
			printf("------------------------------------------------------------->\n");
			pthread_create(&tareas[i], NULL, _array[i].tarea.accion, (void *)_array[i].tarea.proceso);
			for(int j = 0; j < _array[i].tarea.retraso; j++){
				sleep(1);
			}
			pthread_join(tareas[i],NULL);
		}
		else if(_array[i].tarea.estado ==DETENIDO){
			printf("--------------------------------------------------------------\n");
			printf("\t La tarea de nombre [%s], con ID [%d], SE ENCUENTRA EN ESTADO DESACRIVADO\n", _array[i].tarea.nomTarea, _array[i].tarea.id);
			printf("--------------------------------------------------------------\n");
		}
		else{
			;
		}
	}
}

array_task *create_ArrayTasks(void){
	array_task *array = (array_task *)malloc(sizeof(array_task)*MAX_TASK);
	initingArray(array);
	return array;
}

static bool in_ID_tarea(array_task *_array, task _t){
	bool id_ok = true;
	for(int i = 0; i < MAX_TASK; i++){
		if(_array[i].tarea.id == _t.id){
			id_ok = false;
			break;
		}
	}
	return id_ok;
}

static void initingArray(array_task *_array){
	for(int i = 0; i < MAX_TASK; i++){
		_array[i].tarea.id = -1;
		_array[i].empty = true;
	}
}

static int buscar(array_task *_array){
	int _index_empty = -1;
	for(int i = 0; i < MAX_TASK; i++){
		if(_array[i].empty == true){
			_index_empty = i;
			break;
		}
	}
	return _index_empty;
}
