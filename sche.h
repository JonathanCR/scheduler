#ifndef __SCHE_H
#define __SCHE_H

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <pthread.h>

#define MAX_STRING				150
#define MAX_TASK				150

#define UNO						1
#define DOS						2
#define TRES					3
#define CUATRO					4

#define EN_PROCESO						1
#define DETENIDO						0

int contar_tareas;

typedef struct TAREA task;
typedef struct ARREGLO_TAREA array_task;

typedef void *(*TAREA_ACCION)(void *);

struct TAREA{
	int id;
	int retraso;
	char nomTarea[MAX_STRING];
	char proceso[MAX_STRING];
	TAREA_ACCION accion;
	int estado;
};

struct ARREGLO_TAREA{
	task tarea;
	int empty;
};

task *create_tarea(int _id, int _retraso, char *_nomTarea, char *_proceso, int _estado, TAREA_ACCION _accion);
void add_tarea(array_task *_array, task _t);
void run_tarea(array_task *_array);

array_task *create_ArrayTasks(void);

#endif